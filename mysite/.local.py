from pathlib import Path
import os

BASE_DIR = Path(__file__).resolve().parent.parent


DEBUG = True

ALLOWED_HOSTS = []

# SECURITY WARNING: keep the secret key used in production secret!
# SECRET_KEY = 'django-insecure-r4kphg5(y6dx2a5xf_u8fufv*$a4nza&&1vojmdf4+-r9sn^y+'
SECRET_KEY = 'django-insecure-ts6mvvjoff4zs1c5a5s$u-miq4sy3s=uej0w1)7yn$8*7ygl)1'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'vincent',
        'USER': '',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
##### Send Email #####
# DEFAULT_FROM_EMAIL = "MyRGPD.app <donotreply@myrgpd.app>"
# EMAIL_HOST = 'smtp.sendgrid.net'
# EMAIL_HOST_USER = 'apikey' # this is exactly the value 'apikey'
# EMAIL_HOST_PASSWORD = "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"
# EMAIL_PORT = 587
# EMAIL_USE_TLS = True
# #### Console > Email View #####
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Static files (CSS, JavaScript, Images)
STATIC_URL = '/static/'
if DEBUG:
    STATICFILES_DIRS = [os.path.join(BASE_DIR, 'static'), ]
else:
    STATIC_ROOT = os.path.join(BASE_DIR, "static/")