from django import template
from ..models import Post, Comment
from django.db.models import Count

from django.utils.safestring import mark_safe
import markdown

register = template.Library()

@register.simple_tag
def total_posts():
    return Post.published.count()


@register.inclusion_tag('blog/post/latest_posts.html')
def show_latest_posts(count=3):
    latest_posts = Post.published.order_by('-publish')[:count]
    return {'latest_posts': latest_posts}


@register.inclusion_tag('blog/post/latest_comments.html')
def show_latest_comments(count=3):
    latest_comments = Comment.objects.all().order_by('-created')[:count]
    return {'latest_comments': latest_comments}

@register.inclusion_tag('blog/post/publish_draft_posts.html')
def publish_draft_posts(user):
    draft_posts = Post.objects.filter(status="DF")
    return {'draft_posts': draft_posts}


@register.simple_tag
def get_most_commented_posts(count=5):
    return Post.published.annotate(
        total_comments=Count('comments'))\
            .order_by('-total_comments')[:count]


@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text))