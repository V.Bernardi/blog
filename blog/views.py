from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import Post, Comment, AnonymousUser


from django.views.generic import ListView

from .forms import EmailPostForm, CommentForm, PostForm, CommentFormCrispy
from django.core.mail import send_mail

from django.views.decorators.http import require_POST

from taggit.models import Tag

from django.db.models import Count


def post_list(request, tag_slug=None):
    post_list = Post.published.all()
    comment_dict = dict() # dictionnaire -> post, ses commentaires

    print("tag slug: ", tag_slug)

    tag = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        post_list = Post.published.filter(tags__name__in=[tag.name])

    paginator = Paginator(post_list, 3) # 3 Posts par page
    page_number = request.GET.get('page', 1)

    try:
        posts = paginator.page(page_number)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages) # page max
    except PageNotAnInteger:
        posts = paginator.page(1)

    for post in post_list:
        comment_dict[post] = post.comments.filter(active=True)

    # on récupère 3 posts du user courant
    if request.user.is_authenticated:
        post_user = Post.published.filter(author=request.user)[:3]
    else:
        post_user = None

    # les formulaires
    form_post = PostForm()
    form_comment = CommentForm()


    return render(request,
                  'blog/post/list.html',
                  {'user':request.user,
                   'post_user':post_user,
                   'posts': posts,
                   'tag': tag,
                   "comment_dict":comment_dict,
                   'form_post':form_post,
                   'form_comment':form_comment})



def post_detail(request, year, month, day, post):
    post = get_object_or_404(Post,
                             slug=post,
                             publish__year=year,
                             publish__month=month,
                             publish__day=day)

    comments = post.comments.filter(active=True)
    form_comment = CommentForm()

    # posts similaires
    post_tags_ids = post.tags.values_list('id', flat=True)

    similar_posts = Post.published.filter(tags__in=post_tags_ids)\
        .exclude(id=post.id)
    similar_posts = similar_posts.annotate(same_tags=Count('tags'))\
        .order_by('-same_tags','-publish')[:4]

    return render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'comments': comments,
                   'form_comment': form_comment,
                   'similar_posts':similar_posts,
                   "user": request.user})



def post_share(request, post_id):
    post = get_object_or_404(Post, id=post_id, status=Post.Status.PUBLISHED)
    sent = False

    # si le formulaire est envoyé
    if request.method == 'POST':
        form = EmailPostForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            post_url = request.build_absolute_uri(
            post.get_absolute_url())
            subject = f"{cd['name']} recommends you read " \
                f"{post.title}"
            message = f"Read {post.title} at {post_url}\n\n" \
                f"{cd['name']}\'s comments: {cd['comments']}"
            send_mail(subject, message, 'bernardi.vincent28@gmail.com',
                [cd['to']])
            sent = True
    else:
        form = EmailPostForm()
    return render(request, 'blog/post/share.html', {'post': post,
                                                    'form': form,
                                                    "sent": sent})



def post_comment_crispy(request):
    post = None
    comment = None
    form_comment = CommentForm(data=request.POST)

    if form_comment.is_valid():
        comment = form_comment.save()
        comment.save()

        post = comment.post

    return render(request, 'blog/post/comment.html',
                  {'post': post,
                   'form_comment': form_comment,
                   'comment': comment})


def post_comment(request, post_id):
    post = get_object_or_404(Post, id=post_id, status=Post.Status.PUBLISHED)
    comment = None
    form_comment = CommentForm(data=request.POST)

    if form_comment.is_valid():
        comment = form_comment.save(commit=False)
        comment.post = post
        comment.name = request.user.username
        comment.email = 'example@gmail.com',
        comment.save()

    return render(request, 'blog/post/comment.html',
                  {'post': post,
                   'form_comment': form_comment,
                   'comment': comment})



def post_post(request):
    form_post = PostForm(data=request.POST)
    form_comment = CommentForm()
    print("form post", form_post)
    post = None
    if form_post.is_valid():
        post = form_post.save()
        post.save()
        # pour sauvegarder les tags
        # form_post.save_m2m()
    return render(request,
                  'blog/post/detail.html',
                  {'post': post,
                   'form_post':form_post,
                   'form_comment':form_comment})


def publish_draft_posts(request):
    list_posts = request.POST.getlist('posts')
    for post_id in list_posts:
        post = Post.objects.get(id=post_id)
        post.status = 'PB'
        post.save()

    return HttpResponseRedirect(reverse('blog:post_list'))
