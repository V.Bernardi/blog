from django import forms
from django.shortcuts import get_object_or_404
from .models import Comment, Post, User

from django.urls import reverse_lazy
from django.utils.text import slugify
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
from crispy_forms.layout import Layout, Row, Field

from taggit.managers import TaggableManager

class EmailPostForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.add_input(Submit('submit', 'Send e-mail'))
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            Row(
                Field('name', wrapper_class='col-md-2'),
                Field('email', wrapper_class='col-md-2'),
            ),
            Row(
                Field('to', wrapper_class='col-md-2'),
            ),
            Row(
                Field('comments', wrapper_class='col-md-2'),
            )
        )

    name = forms.CharField(max_length=25, label="name")
    email = forms.EmailField(label="email")
    to = forms.EmailField(label="to")
    comments = forms.CharField(required=False,
                               widget=forms.Textarea(attrs={'placeholder':'Your comment goes here'}),
                               label="comment",)


class CommentFormCrispy(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_action = reverse_lazy('blog:post_comment')
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.form_show_errors = True

    post_id = forms.IntegerField(widget=forms.HiddenInput)
    body = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder':'Add comment'})
    )

    def save(self):
        cleaned_data = self.cleaned_data

        body = cleaned_data['body']
        print("cleaned data: ", cleaned_data)
        post_id = cleaned_data['post_id']

        post = get_object_or_404(Post, id=post_id, status=Post.Status.PUBLISHED)
        print("post", post)

        try:
            comment = Comment.objects.create(
                post = post,
                name =  ' '.join(body.split()[:5]) + " ..." , # on prend les 5 premiers mots du commentaire
                email = 'example@gmail.com',
                body = body,
            )
            return comment
        
        except:
            # envoyer une erreur 404
            pass


class CommentForm(forms.ModelForm):
    body = forms.CharField(label="",
                           widget=forms.Textarea(attrs={'placeholder':'Your comment goes here'}))

    class Meta:
        model = Comment
        fields = [ 'body']



class PostForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_action = reverse_lazy('blog:post_create')
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.form_show_errors = True
        self.helper.layout = Layout(
            Row(
                Field('title', wrapper_class='col-md-9'),
                Field('author', wrapper_class='col-md-2'),
            ),
            Row(
                Field('body')
            ),
            Row(
                Field('status', wrapper_class='col-md-4'),
                Field('tags', wrapper_class='col-md-7')
            )
        )

    title = forms.CharField(label="title")
    author = forms.ChoiceField(choices=[
            (user.id, user.username) for user in User.objects.all()
        ],
        widget=forms.Select(),
        initial=1,
        label="author"
    )
    body = forms.CharField(
        widget=forms.Textarea(attrs={'placeholder':'Your post goes here'}),
        label="body"
    )
    status = forms.ChoiceField(choices=
        ((1, 'Draft'),
        (2, 'Published'),),
        widget=forms.RadioSelect(),
        initial=1,
        label="status"
    )
    tags = forms.CharField(
        required=False, help_text="Separate tags with a comma",
        label="tags"
    )

    def save(self):
        cleaned_data = self.cleaned_data
        
        title = cleaned_data['title']
        author = User.objects.get(pk=cleaned_data['author'])
        body = cleaned_data['body']
        status = ['DF', 'PB'][int(cleaned_data['status'])-1]
        tags = cleaned_data['tags'].split(',')

        post = Post.objects.create(
            title=title,
            slug=slugify(title),
            author=author,
            body=body,
            status=status,
            tags=tags
        )

        return post



